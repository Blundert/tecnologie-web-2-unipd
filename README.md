![71839_508821162473694_30535643_n.jpg](https://bitbucket.org/repo/ELgjEb/images/1480710335-71839_508821162473694_30535643_n.jpg)

# Deerwaves

[Deer Waves](http://deerwaves.com/) è un sito, una redazione, un novero di autori e critici, un team, un gruppo di lavoro e di amici che condivide la passione per la musica. Crediamo che l’informazione musicale debba essere più diretta, più leggera, più interessante e soprattutto meno retorica.

- - -

# Indice sezioni del documento #

* Introduzione
* Considerazioni generali  
    * Design
    * Navigazione
    * Testo
    * Ricerca
    * Immagini
    * Pubblicità
    * Gestione errore 404
* Homepage  
    * Where?
    * Who?
    * Why?
    * What?
    * When?
    * How?
* Pagine interne  
    * 6W nella pagine interne 
* Pagine con il layout della sezione "Recensioni album"  
  (Recensioni album, Mixtapes)
* Pagina di articolo
* Pagine con il layout della sezione "Hype"  
  (Hype, News, Live report, Inchieste, Hasta la lista, Geek deer, Classifiche, Contest)
* Pagina "Chi siamo"
* Mobile  
    * 6W nella versione mobile
    * Navigazione 
    * Testo
    * Ricerca
    * Homepage
* Conclusioni 
* Appendice "Licenza per Documentazione Libera GNU"
* Appendice "Lista delle Figure"


- - -

Licenza [GFDL](https://bitbucket.org/Blundert/tecnologie-web-2-unipd/src/e7fabf145bb3ccb84e93d510f01ce3ad01d1ecb6/LICENSE?fileviewer=file-view-default)

- - -


Copyright (c) 2016 - [Matteo Granzotto](http://www.matteogranzotto.com/)